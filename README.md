<div align="center">

![](/assets/loki_full_documentation_small.png "loki")

<br>
<img src="https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white">
<img src="https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white">
<img src="https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E">
<img src="https://img.shields.io/badge/vuejs-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D">
<img src="https://img.shields.io/badge/NPM-%23000000.svg?style=for-the-badge&logo=npm&logoColor=white">
<img src="https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white">
<br><br>
<a href="#"><img src="https://img.shields.io/badge/build-passing-green"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
<br><br>
<i><b>Documentation for <a href="https://gitlab.com/rodrigoodhin/loki">loki webservice</a></b></i>
<br><br>
</div>

#### Prerequisites
- [Node@14.17.6](https://nodejs.org/en/)
- [Yarn](https://classic.yarnpkg.com/en/)

#### Steps to clone project and start loki doc server
```shell
yarn install
npm install
npm run serve
```

#### Compiles and minifies for production
```shell
npm run build
```

### Lints and fixes files
```shell
npm run lint
```

#### You can check this project running at: [rodrigoodhin.gitlab.io/loki-doc](https://rodrigoodhin.gitlab.io/loki-doc/)
